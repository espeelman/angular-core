﻿using AngularCore.Authorization.Exceptions;
using AngularCore.Entities;
using Microsoft.AspNetCore.Cryptography.KeyDerivation;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Security.Cryptography;
using System.Threading.Tasks;

namespace AngularCore.Authorization
{
    /// <summary>
    /// UserManager handles user adminstration including authenciation
    /// </summary>
    public class UserManager : IUserManager
    {
        public UserManager(AngularCoreContext context)
        {
            _context = context;
        }
        /// <summary>
        /// Creates a session based on the login and password
        /// </summary>
        /// <param name="login">Users username or email</param>
        /// <param name="password">Users password</param>
        /// <param name="sessionLength">The number of seconds the session should be valid for</param>
        /// <returns>Session object or null if the credentials are bad</returns>
        public async Task<Session> CreateSession(string? login, string? password, int sessionLength)
        {
            login = login?.Trim();
            var user = await _context.Users.Where(x => x.Email == login || x.Username == login).SingleOrDefaultAsync();
            if (user == null)
            {
                throw new SessionCreationException();
            }
            var hashedPassword = HashPassword(password, user.Salt!);
            if (hashedPassword != user.PasswordHash)
            {
                throw new SessionCreationException();
            }
            var now = DateTime.UtcNow;
            var session = new Session()
            {
                CreatedOn = now,
                ExpiresOn = now.AddSeconds(sessionLength),
                User = user,
            };
            _context.Sessions!.Add(session);
            await _context.SaveChangesAsync();
            return session;
        }
        /// <summary>
        /// Gets an active session
        /// </summary>
        /// <param name="id">Session Id to retrieve</param>
        /// <param name="minLength">Miniumn number of seconds session will be valid for. Will adjust exipration d/t if necessary</param>
        /// <returns>Session object</returns>
        public async Task<Session> GetSession(Guid id, int minLength = 0)
        {
            var now = DateTime.UtcNow;
            var session = await _context.Sessions.Include(x => x.User).SingleOrDefaultAsync(x => x.Id == id);
            if (session == null)
            {
                throw new ArgumentException("Session does not exist");
            }
            if(session.ExpiresOn < now)
            {
                throw new SessionExpiredException();
            }
            var expiration = DateTime.UtcNow.AddSeconds(minLength);
            if (expiration > session.ExpiresOn)
            {
                session.ExpiresOn = expiration;
                await _context.SaveChangesAsync();
            }
            return session;
        }
        /// <summary>
        /// Deletes session
        /// </summary>
        /// <param name="id">Session Id of session to be deleted</param>
        /// <returns></returns>
        public async Task DeleteSession(Guid id)
        {
            var session = _context.Sessions.Where(x => x.Id == id).SingleOrDefault();
            if(session == null)
            {
                throw new ArgumentException("Session does not exist");
            }
            _context.Sessions!.Remove(session);
            await _context.SaveChangesAsync();
        }
        /// <summary>
        /// Creates a new user
        /// </summary>
        /// <param name="username">username of new user</param>
        /// <param name="email">email of new user</param>
        /// <param name="password">password of new user</param>
        /// <returns>user object of newly created user</returns>
        public async Task<User> CreateUser(string username, string email, string password)
        {
            email = email.Trim();
            username = username.Trim();
            if(_context.Users.Any(x => x.Username == username))
            {
                throw new UsernameInUseException();
            }
            if (_context.Users.Any(x => x.Email == email))
            {
                throw new EmailInUseException();
            }
            var salt = GenerateSalt();
            var user = new User()
            {
                Email = email,
                PasswordHash = HashPassword(password, salt),
                Salt = salt,
                Username = username,
                CreatedOn = DateTime.UtcNow
            };
            _context.Users!.Add(user);
            await _context.SaveChangesAsync();
            return user;
        }

        /// <summary>
        /// Current logged in user
        /// </summary>
        public Session? CurrentSession { get; set; }
        private async Task<Session> GetSession(Guid id, bool includeUser = true)
        {
            var sessionQuery = _context.Sessions.Where(x => x.Id == id);
            if(includeUser)
            {
                sessionQuery.Include(x => x.User);
            }
            var session = await sessionQuery.SingleOrDefaultAsync();
            if(session == null)
            {
                throw new ArgumentException("Session Id does not exist");
            }
            return session;
        }

        private static string GenerateSalt()
        {
            byte[] salt = new byte[128 / 8];
            using (var rng = RandomNumberGenerator.Create())
            {
                rng.GetBytes(salt);
            }
            return Convert.ToBase64String(salt);
        }

        private static string HashPassword(string? password, string salt)
        {
            var key = KeyDerivation.Pbkdf2(password, Convert.FromBase64String(salt), KeyDerivationPrf.HMACSHA1, 10000, 256 / 8);
            return Convert.ToBase64String(key);
        }

        private static bool IsValidEmail(string email)
        {
            try
            {
                var addr = new System.Net.Mail.MailAddress(email);
                return addr.Address == email;
            }
            catch
            {
                return false;
            }
        }

        private readonly AngularCoreContext _context;
    }
}
