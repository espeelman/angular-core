﻿using AngularCore.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace AngularCore.Authorization
{
    public interface IUserManager
    {
        Task<Session> CreateSession(string? login, string? password, int sessionLength);
        Task<Session> GetSession(Guid id, int minLength = 0);
        Task DeleteSession(Guid id);
        Task<User> CreateUser(string username, string email, string password);
        Session? CurrentSession { get; set; }
    }
}
