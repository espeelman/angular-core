using AngularCore.Authorization.Exceptions;
using AngularCore.Entities;
using AngularCore.Tests.Helpers;
using System;
using System.Threading.Tasks;
using Xunit;

namespace AngularCore.Authorization.Tests
{
    public class UserManagerUnitTest
    {
        public UserManagerUnitTest()
        {
            context = InMemoryContext.Create();
            userManager = new UserManager(context);
        }
        [Fact]
        public async Task CanCreateUser()
        {
            var user = await userManager.CreateUser("test", "test@test.com", "password");
            Assert.Equal("test", user.Username);
            Assert.Equal("test@test.com", user.Email);
        }

        [Fact]
        public async Task CantCreateDuplicateUsers()
        {
            await userManager.CreateUser("dupe", "dupe@test.com", "password");
            await Assert.ThrowsAsync<UsernameInUseException>(() => userManager.CreateUser("dupe", "new@email.com", "password"));
            await Assert.ThrowsAsync<EmailInUseException>(() => userManager.CreateUser("new", "dupe@test.com", "password"));
        }

        private readonly AngularCoreContext context;
        private readonly UserManager userManager;
    }
}
