﻿using AngularCore.Entities;
using Microsoft.EntityFrameworkCore;
using System;

namespace AngularCore.Tests.Helpers
{
    public class InMemoryContext
    {
        public static AngularCoreContext Create()
        {
            var optionsBuilder = new DbContextOptionsBuilder<AngularCoreContext>();
            optionsBuilder.UseInMemoryDatabase("AngularCore");
            return new AngularCoreContext(optionsBuilder.Options);
        }

    }
}
