﻿using System.Threading.Tasks;
using AngularCore.Api.Attributes;
using AngularCore.Api.Models;
using AngularCore.Authorization;
using AngularCore.Authorization.Exceptions;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;

namespace AngularCore.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UsersController : ControllerBase
    {
        public UsersController(IMapper mapper, IUserManager userManager)
        {
            _userManager = userManager;
            _mapper = mapper;
        }

        /// <summary>
        /// Creates a new user.
        /// </summary>
        /// <param name="userModel">Class that describes user to be created</param>
        /// <returns>Newly created user object</returns>
        [Anonymous]
        [HttpPost]
        public async Task<ActionResult<UserModel>> Post(CreateUserModel userModel)
        {
            try
            {
                var user = await _userManager.CreateUser(userModel.Username!, userModel.Email!, userModel.Password!);
                var session = await _userManager.CreateSession(user.Email, userModel.Password, 24);
                return Ok(_mapper.Map<SessionModel>(session));
            }
            catch(UsernameInUseException)
            {
                ModelState.AddModelError("username", "Username is already in use");
            }catch(EmailInUseException)
            {
                ModelState.AddModelError("email", "Email is already in use");
            }
            return BadRequest(ModelState);
        }
        /// <summary>
        /// Gets current logged in user based on token
        /// </summary>
        /// <returns>User object</returns>
        [Route("me")]
        [HttpGet]
        public ActionResult<UserModel> Get()
        {
            return Ok(_mapper.Map<UserModel>(_userManager.CurrentSession!.User));
        }

        private readonly IUserManager _userManager;
        private readonly IMapper _mapper;

    }
}
