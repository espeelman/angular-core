﻿using System.Threading.Tasks;
using AngularCore.Api.Attributes;
using AngularCore.Api.Models;
using AngularCore.Authorization;
using AngularCore.Authorization.Exceptions;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;

namespace AngularCore.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class SessionsController : ControllerBase
    {
        public SessionsController(IMapper mapper, IUserManager userManager)
        {
            _userManager = userManager;
            _mapper = mapper;
        }

        /// <summary>
        /// Creates a session based on the login and password
        /// </summary>
        /// <param name="sessionModel">Class that contains login and password</param>
        /// <returns>returns session object or 401 if credentials are incorrect</returns>
        [HttpPost]
        [Anonymous]
        public async Task<ActionResult<SessionModel>> Post(CreateSessionModel sessionModel)
        {
            try
            {
                var session = await _userManager.CreateSession(sessionModel.Login, sessionModel.Password, 3600);
                return Ok(_mapper.Map<SessionModel>(session));
            }catch(SessionCreationException)
            {
                return Unauthorized();
            }
        }
        /// <summary>
        /// Deletes current session, invalidating token
        /// </summary>
        /// <returns></returns>
        [HttpDelete]
        public async Task<ActionResult> Delete()
        {
            await _userManager.DeleteSession(_userManager.CurrentSession!.Id);
            return Ok();
        }

        private readonly IUserManager _userManager;
        private readonly IMapper _mapper;
    }
}
