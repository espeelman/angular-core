﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AngularCore.Api.Attributes
{
    /// <summary>
    /// The anonymous will allow a controller class or method to be called without checking for authorization
    /// </summary>
    [AttributeUsage(AttributeTargets.Method | AttributeTargets.Class, Inherited = false, AllowMultiple = false)]
    public class Anonymous : Attribute
    {
    }
}
