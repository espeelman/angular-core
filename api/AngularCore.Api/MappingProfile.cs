﻿using AngularCore.Api.Models;
using AngularCore.Entities;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AngularCore.Api
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateMap<User, UserModel>();
            CreateMap<Session, SessionModel>()
                .ForMember(m => m.Email, o => o.MapFrom(s => s.User!.Email))
                .ForMember(m => m.Username, o => o.MapFrom(s => s.User!.Username));

        }
    }
}
