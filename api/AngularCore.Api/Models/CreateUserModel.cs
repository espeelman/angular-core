﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace AngularCore.Api.Models
{
    public class CreateUserModel
    {
        [Required(ErrorMessage = "Username is required")]
        [StringLength(50, MinimumLength = 2, ErrorMessage = "Username must be between 2 and 50 characters")]
        public string? Username { get; set; }
        [Required(ErrorMessage = "Email is required")]
        [EmailAddress(ErrorMessage = "Email address is not valid")]
        public string? Email { get; set; }
        [Required]
        [StringLength(50, MinimumLength = 7, ErrorMessage = "Password must be between 2 and 50 characters")]
        public string? Password { get; set; }
    }
}
