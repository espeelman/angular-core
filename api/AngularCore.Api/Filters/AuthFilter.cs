﻿using AngularCore.Api.Attributes;
using AngularCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Controllers;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AngularCore.Api.Filters
{
    /// <summary>
    /// Checks for proper authorization and short circuits the route returning a 401 if none is found. 
    /// It looks for a bearer token in the Authorization header and used that to retrieve an active session. 
    /// It will allow requests to pass through without authorization if the controller class or method has the Anonymous attribute.
    /// </summary>
    public class AuthFilter : IAsyncActionFilter
    {
        public AuthFilter(ILogger<AuthFilter> logger, IUserManager userManager)
        {
            _userManager = userManager;
            _logger = logger;
        }
        public async Task OnActionExecutionAsync(ActionExecutingContext context, ActionExecutionDelegate next)
        {
            var controllerDescriptor = context.ActionDescriptor as ControllerActionDescriptor;
            if (controllerDescriptor!.MethodInfo.GetCustomAttributes(typeof(Anonymous), false).Any() ||
                controllerDescriptor!.ControllerTypeInfo.GetCustomAttributes(typeof(Anonymous), false).Any())
            {
                await next();
            }
            else
            {
                var authHeader = context.HttpContext.Request.Headers["Authorization"].FirstOrDefault();
                if (!string.IsNullOrWhiteSpace(authHeader))
                {
                    authHeader = authHeader.Trim();
                    var authHeaderParts = authHeader.Split(' ');
                    if (authHeaderParts.Length == 2 && Guid.TryParse(authHeaderParts[1], out Guid sessionId))
                    {
                        try
                        {
                            var session = await _userManager.GetSession(sessionId, 3600);
                            _userManager.CurrentSession = session;
                            await next();
                        }
                        catch (Exception ex)
                        {
                            throw new Exception("An unexpected exception occured durning authenciation", ex);
                        }
                    }
                }
            }
            context.Result = new UnauthorizedResult();
        }
        private readonly IUserManager _userManager;
        private readonly ILogger<AuthFilter> _logger;
    }
}
