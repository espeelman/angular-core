﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace AngularCore.Entities
{
    public class User
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key]
        public Guid Id { get; set; }
        [Required]
        [EmailAddress]
        [StringLength(254)]
        public string? Email { get; set; }
        [Required]
        [StringLength(50, MinimumLength = 2)]
        public string? Username { get; set; }
        [Required]
        [StringLength(24, MinimumLength = 24)]
        [Column(TypeName = "nchar(44)")]
        public string? Salt { get; set; }
        [Required]
        [StringLength(44, MinimumLength = 44)]
        [Column(TypeName = "nchar(44)")]
        public string? PasswordHash { get; set; }
        [Required]
        public DateTime CreatedOn { get; set; }
    }   
}
