﻿using Microsoft.EntityFrameworkCore;
using System;

namespace AngularCore.Entities
{
    public class AngularCoreContext : DbContext
    {
        public AngularCoreContext(DbContextOptions<AngularCoreContext> options) : base(options)
        {
        }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<User>().HasAlternateKey(x => x.Email);
            modelBuilder.Entity<User>().HasAlternateKey(x => x.Username);
            modelBuilder.Entity<User>().HasData(new User()
            {
                Id = Guid.Parse("4154A39D-17F2-4D16-7473-08D595E87004"),
                Email = "eric.t.speelman@gmail.com",
                Username = "Eric",
                PasswordHash = "FiYvysyQc9b59R4LEjJiLzPgHlpjNhv+lf7Tlu2rkYk=",
                Salt = "8jhIfRquM689LPZ9DzOIWg=="
            });
        }

        public DbSet<User>? Users { get; set; }
        public DbSet<Session>? Sessions { get; set; }
    }
}
