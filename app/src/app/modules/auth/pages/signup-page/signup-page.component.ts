import { Component, ChangeDetectionStrategy, OnInit, OnDestroy } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { CustomValidators, FormErrorService, AuthService } from '../../../../core';
import { Router } from '@angular/router';
import { Observable, BehaviorSubject } from 'rxjs';

@Component({
  selector: 'ac-signup-page',
  templateUrl: './signup-page.component.html',
  styleUrls: ['./signup-page.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class SignupPageComponent implements OnInit, OnDestroy {
  signupForm: FormGroup;
  errors$: Observable<any>;
  loading$ = new BehaviorSubject<boolean>(false);
  constructor(private fb: FormBuilder, private errorService: FormErrorService, private authService: AuthService,
              private router: Router) {
  }

  ngOnInit() {
    this.signupForm = this.fb.group({
      username: ['', [Validators.required, Validators.minLength(2), Validators.maxLength(50)]],
      email: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required, Validators.minLength(7), Validators.maxLength(50)]],
      verifyPassword: ['', [CustomValidators.match('password')]]
    });
    this.errors$ = this.errorService.observe(this.signupForm);
  }

  signup() {
    this.loading$.next(true);
    this.authService.signup(this.signupForm.controls.username.value, this.signupForm.controls.email.value,
      this.signupForm.controls.password.value).subscribe(session => {
        this.router.navigate(['']);
      }, err => {
        this.errorService.setErrors(this.signupForm, err);
        this.loading$.next(false);
      });
  }
  ngOnDestroy() {
    CustomValidators.unsubscribe();
  }
}
