import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { Router } from '@angular/router';
import { FormGroup, FormBuilder } from '@angular/forms';
import { AuthService } from '../../../../core';
import { BehaviorSubject } from 'rxjs';

@Component({
  selector: 'ac-login-page',
  templateUrl: './login-page.component.html',
  styleUrls: ['./login-page.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class LoginPageComponent implements OnInit {
  loginForm: FormGroup;
  showError$ = new BehaviorSubject(false);
  loading$ = new BehaviorSubject(false);
  constructor(private fb: FormBuilder, private authService: AuthService, private router: Router) {
  }

  ngOnInit() {
    this.loginForm = this.fb.group({
      login: '',
      password: ''
    });
  }

  login() {
    this.loading$.next(true);
    this.authService.login(this.loginForm.controls.login.value, this.loginForm.controls.password.value)
      .subscribe(() => this.router.navigate(['']),
      () => {
        this.showError$.next(true);
        this.loading$.next(false);
      });
  }

  signup() {
    this.router.navigate(['signup']);
  }

}
