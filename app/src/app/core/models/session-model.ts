export class SessionModel {
  id: string;
  email: string;
  username: string;
  createdOn: Date;
  expiresOn: Date;

  static create(json: any): SessionModel {
    const session = new SessionModel();
    session.id = json.id;
    session.email = json.email;
    session.username = json.username;
    session.createdOn = new Date(json.createdOn);
    session.expiresOn = new Date(json.expiresOn);
    return session;
  }
}
