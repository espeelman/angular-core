import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { HttpService, SessionService, FormErrorService } from './services';
import { AuthGuard } from './guards';

@NgModule({
  imports: [
    CommonModule,
    HttpClientModule
  ],
  declarations: [],
  providers: [HttpService, SessionService, FormErrorService, AuthGuard]
})
export class CoreModule { }
