import { Injectable } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { map, shareReplay, startWith } from 'rxjs/operators';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class FormErrorService {
  private validate(form: FormGroup) {
    const errors = {};
    for (const controlName in form.controls) {
      if (controlName) {
        const control = form.controls[controlName];
        if (!control.valid) {
          for (const errorKey in control.errors) {
            if (errorKey) {
              errors[controlName] = this.getErrorMessage(errorKey, control.errors);
            }
          }
        }
      }
    }
    return errors;
  }

  observe(form: FormGroup) {
    return form.valueChanges.pipe(
      startWith(0),
      map(() => this.validate(form)),
      shareReplay()
    );
  }

  setErrors(form: FormGroup, errors: any) {
    for (const controlName in errors) {
      if (form.controls[controlName]) {
        form.controls[controlName].setErrors(errors[controlName]);
      }
    }
    form.updateValueAndValidity({emitEvent: true});
  }

  private getErrorMessage(key: string, error: any) {
    console.log('key');
    if (key === 'required') {
      return 'This field is required';
    } else if (key === 'minlength') {
      return `This field must be at least ${error.minlength.requiredLength} characters`;
    } else if (key === 'maxlength') {
      return `This field can't be longer than ${error.maxlength.requiredLength} characters`;
    } else if (key === 'email') {
      return 'This field must be a valid email address';
    } else if (key === 'match') {
      return 'This fields must match';
    } else if (key === '0') {
      return error[0];
    } else {
      return 'This field is invalid';
    }
  }
}
