import { Injectable } from '@angular/core';
import { SessionModel } from '../../models';

@Injectable({
  providedIn: 'root'
})
export class SessionService {
  private session: SessionModel;
  constructor() {
    this.getSession();
  }

  endSession() {
    window.localStorage.removeItem('session');
    this.session = null;
  }

  setSession(session: SessionModel) {
    window.localStorage.setItem('session', JSON.stringify(session));
    this.session = session;
  }

  getSession() {
    if (this.session) {
      return this.session;
    }
    const sessionString = window.localStorage.getItem('session');
    if (sessionString) {
      this.session = SessionModel.create(JSON.parse(sessionString));
      return this.session;
    }
    return null;
  }

  extendSession(seconds: number) {
    if (this.session) {
      const expiration = new Date();
      expiration.setSeconds(expiration.getSeconds() + seconds);
      if (expiration > this.session.expiresOn) {
        this.session.expiresOn = expiration;
        this.setSession(this.session);
      }
    }
  }

  hasValidSession() {
    return this.session && this.session.expiresOn.getTime() >= Date.now();
  }
}
