import { Injectable } from '@angular/core';
import { HttpService } from '../http/http.service';
import { SessionModel } from '../../models';
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';
import { SessionService } from '../session/session.service';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(private http: HttpService, private sessionService: SessionService) { }

  login(login: string, password: string): Observable<SessionModel> {
    return this.http.post<SessionModel>('api/sessions', {login: login, password: password}, SessionModel.create, false).pipe(
      tap(session => this.sessionService.setSession(session))
    );
  }

  signup(username: string, email: string, password: string): Observable<SessionModel> {
    return this.http.post<SessionModel>('api/users', {username: username, email: email, password: password }, SessionModel.create, false)
      .pipe(
        tap(session => this.sessionService.setSession(session))
      );
  }
}
