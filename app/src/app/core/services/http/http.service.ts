import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse, HttpRequest, HttpErrorResponse, HttpEventType } from '@angular/common/http';
import { Router } from '@angular/router';
import { environment } from '../../../../environments/environment';
import { Observable, empty, throwError } from 'rxjs';
import { map, catchError, filter, tap } from 'rxjs/operators';
import { SessionService } from '../session/session.service';
import { MethodCall } from '@angular/compiler';

@Injectable({
  providedIn: 'root'
})
export class HttpService {
  readonly apiUrl = environment.apiUrl;

  constructor(private http: HttpClient, private router: Router, private sessionService: SessionService) { }

  post<T>(path: string, body: any, create: (json: any) => T,  authorize = true): Observable<T> {
    return this.request('POST', path, body, create, authorize);
  }

  request<T>(method: string, path: string, body: any, create: (json: any) => T, authorize = true) {
    if (!create) {
      create = x => x;
    }
    const request = new HttpRequest(method, this.apiUrl + path, body, {
      withCredentials: false
    });
    return this.http.request<T>(request).pipe(
      filter(response => response.type === HttpEventType.Response),
      map(response => create((<HttpResponse<T>>response).body)),
      tap(() => this.sessionService.extendSession(3600)),
      catchError(error => this.handleError(error, authorize))
    );
  }

  // TODO add better error handling
  private handleError(error: HttpErrorResponse, authorize: boolean) {
    if (error.status === 400) {
      return throwError(error.error);
    } else if (error.status === 401) {
      if (authorize) {
        this.router.navigate(['login']);
      }
      return throwError(error);
    } else if (error.error instanceof ErrorEvent) {
      console.log(`Client side error: ${error.error}`);
      return empty();
    } else {
      console.log(`A server error has occured: ${error.error}`);
      return empty();
    }
  }
}
