import { FormControl, ValidationErrors, FormGroup } from '@angular/forms';
import { Subscription } from 'rxjs';

export class CustomValidators {
  private static subscriptions: Subscription[] = [];
  static match(matchingControl: string) {
    let parent: FormGroup;
    return function(control: FormControl): ValidationErrors {
      if (control.parent) {
        if (!parent) {
          parent = <FormGroup>control.parent;
          CustomValidators.subscriptions.push(
            parent.controls[matchingControl].valueChanges.subscribe(() => {
              control.updateValueAndValidity();
            }));
        }
        if (parent.controls[matchingControl].value !== control.value) {
          return {match: 'The input values do not match'};
        }
      }
      return null;
    };
  }

  static unsubscribe() {
    for (const sub of this.subscriptions) {
      sub.unsubscribe();
    }
  }
}



