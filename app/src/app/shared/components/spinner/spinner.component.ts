import { Component, ChangeDetectionStrategy, Input, ElementRef, ViewChild, OnInit, OnDestroy } from '@angular/core';
import { Observable, BehaviorSubject } from 'rxjs';
import { tap } from 'rxjs/operators';

@Component({
  selector: 'ac-spinner',
  templateUrl: './spinner.component.html',
  styleUrls: ['./spinner.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class SpinnerComponent {
  @Input() set spin(spin: boolean) {
    console.log(spin + ' ' + this.lock);
    if (spin && this.lock) {
      const height = Math.max(100, this.spinnerContainer.nativeElement.offsetHeight);
      const width = Math.max(100, this.spinnerContainer.nativeElement.offsetWidth);
      this.spinner.nativeElement.style.height = height + 'px';
      this.spinner.nativeElement.style.width = width + 'px';
    } else if (this.lock) {
      this.spinner.nativeElement.style.height = null;
      this.spinner.nativeElement.style.width = null;
    }
    this.shouldSpin$.next(spin);
  }
  @Input() lock = false;
  @ViewChild('spinner') spinner: ElementRef;
  @ViewChild('spinnerContainer') spinnerContainer: ElementRef;
  public shouldSpin$: BehaviorSubject<boolean> = new BehaviorSubject(false);
}
