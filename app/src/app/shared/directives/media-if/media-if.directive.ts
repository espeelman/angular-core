import { Directive, Input, TemplateRef, ViewContainerRef, OnInit, OnDestroy} from '@angular/core';
import { ObservableMedia, MediaChange } from '@angular/flex-layout';
import { Subscribable, Subscription } from 'rxjs';

@Directive({
  selector: '[acMediaIf]'
})
export class MediaIfDirective implements OnInit, OnDestroy {
  private mediaStates = ['xs', 'sm', 'md', 'lg', 'xl'];
  private mediaState: string;
  private subscription: Subscription;
  @Input() appMediaIf: string;
  constructor(public media: ObservableMedia, private templateRef: TemplateRef<any>, private viewContainer: ViewContainerRef) {

  }
  ngOnInit() {
    this.subscription = this.media.asObservable()
    .subscribe((change: MediaChange) => {
      if (change.mqAlias !== this.mediaState) {
        this.mediaState = change.mqAlias;
        this.render();
      }
    });
    this.render();
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  private render() {
    let shouldRender = false;
    const mediaIndex = this.mediaStates.indexOf(this.mediaState);
    if (this.appMediaIf && mediaIndex > -1) {
      const mediaParts = this.appMediaIf.split('-').map(x => x.trim().toLowerCase());
      if (mediaParts.length === 1) {
        shouldRender = this.mediaState === mediaParts[0];
      } else if (mediaParts.length === 2) {
        const minIndex = this.mediaStates.indexOf(mediaParts[0]);
        const maxIndex = this.mediaStates.indexOf(mediaParts[1]);
        shouldRender = (minIndex === -1 || minIndex <= mediaIndex) && (maxIndex === -1 || maxIndex >= mediaIndex);
      }
    }
    this.viewContainer.clear();
    if (shouldRender) {
      this.viewContainer.createEmbeddedView(this.templateRef);
    }
  }
}


